﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sweet16Ritt6.Startup))]
namespace Sweet16Ritt6
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
